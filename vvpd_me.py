import argparse


def new_function():
    print('Hello word')

def check_num(date):
    """
    Проверка правильности даты
    :param date: дата которую нужно проверить
    :return: существует или не существует дата
    """
    if (0 < date[0] < 32) and (0 < date[1] < 13) and (1999 < date[2] < 10000):
        return True
    else:
        return False


def check_date(num):
    """
    Преобразует дату всоответствии с ее типом
    :param num: дата
    :return: дата
    """
    if type(num) == str:  # '10.12.2022'
        return help_function(num)

    elif type(num) == list and len(num) == 1:  # ['12.12.2022']
        num = num[0]
        return help_function(num)

    elif type(num) == list and not len(num) % 2:  # ['Иван', '12.12.2022', 'Коля', '03.12.2022']
        num_dict = {}
        for i in range(0, len(num), 2):
            num_dict[num[i]] = help_function(num[i+1])
        return num_dict


def help_function(num):
    """
    Gреобразует дату из строки в список
    :param num: дата 'DD.MM.YYYY'
    :return: [DD, MM, YYYY]
    """
    num = num.split('.')  # ['10', '12', '2022']
    for i, val in enumerate(num):
        try:
            int(val)
        except ValueError:
            return 'Введена некоректная дата'
        else:
            num[i] = int(val)  # [10, 12, 2022]
    return num


def deadline_score(delivery, deadline):  # [12, 12, 2022] [10, 12, 2022]
    """
    Посчет оценки исходя из даты сдачи работы
    :param delivery: дата сдачи
    :param deadline: дата дедлайна
    :return: оценку работы
    """
    if check_num(deadline) and check_num(delivery):
        if delivery[1] > deadline[1] or delivery[2] > deadline[2]:
            print(0)
        elif delivery[1] < deadline[1] or delivery[2] < deadline[2]:
            print('Вы предсказываете будующее, 5')
        elif delivery[0] <= deadline[0]:
            print(5)
        else:
            score = delivery[0] - deadline[0]
            if score >= 5:
                print(0)
            else:
                print(5 - score)
    else:
        print('Дата несуществующая')


def late_list(delivery, deadline):
    """
    Определяет список опоздавших со сдачей работы
    :param delivery: дата сдачи
    :param deadline: дата дедлайна
    :return: список опаздавших
    """
    debtors = []
    for i, (key, val) in enumerate(delivery.items()):
        if val[0] > deadline[0] or val[1] > deadline[1] or val[2] > deadline[2]:
            debtors.append(key)
    debtors.sort()
    print(debtors)


def main():
    """
    Главная функция
    :return: сколько баллов вы получите или список должников
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--delivery', nargs='+', type=str)
    parser.add_argument('--deadline', type=str)
    args = parser.parse_args()
    delivery = check_date(args.delivery)  # [12, 14, 2022] \ {'Ваня': [12, 23, 3000]}
    deadline = check_date(args.deadline)  # [12, 13, 2022]

    if type(delivery) == list and type(deadline) == list:
        deadline_score(delivery, deadline)

    if type(delivery) == dict and type(deadline) == list:
        late_list(delivery, deadline)

    elif type(delivery) == str or type(deadline) == str:
        print('Введена некоректная дата')


if __name__ == '__main__':
    main()
